package kz.nedb.esb.oauth.client

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class OAuthClientApplication

fun main(args: Array<String>) {
    runApplication<OAuthClientApplication>(* args)
}
