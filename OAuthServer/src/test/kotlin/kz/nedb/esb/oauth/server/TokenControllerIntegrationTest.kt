package kz.nedb.esb.oauth.server

import kz.nedb.esb.oauth.server.controller.TokenController
import org.assertj.core.api.Assertions.assertThat
import org.codehaus.jackson.map.ObjectMapper
import org.codehaus.jackson.type.TypeReference
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import java.util.ArrayList
import java.util.stream.IntStream

import org.mockito.Mockito.`when`
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = [OAuthServerApplication::class, TokenController::class]
)
open class TokenControllerIntegrationTest {

    @Autowired
    lateinit var sut: TokenController

    @Autowired
    lateinit var mockMvc: MockMvc

    @MockBean
    lateinit var tokenStore: TokenStore

    val mapper = ObjectMapper()

    @Test
    fun shouldLoadContext() {
        assertThat(sut).isNotNull()
    }

    @Test
    fun shouldReturnEmptyListOfTokens() {
        val tokens = retrieveTokens()

        assertThat<String>(tokens).isEmpty()
    }

    @Test
    fun shouldReturnNotEmptyListOfTokens() {

        `when`(tokenStore.findTokensByClientId("sampleClientId")).thenReturn(generateTokens(1))

        val tokens = retrieveTokens()

        assertThat(tokens).hasSize(1)
    }


    open fun retrieveTokens(): List<String> =
            mapper.readValue(mockMvc.perform(get("/tokens")).andExpect(status().isOk()).andReturn().response.contentAsString, object : TypeReference<List<String>>() {

            })

    open fun generateTokens(count: Int): List<OAuth2AccessToken> {
        var result = ArrayList<OAuth2AccessToken>()
        IntStream.range(0, count).forEach { n -> result.add(DefaultOAuth2AccessToken("token$n")) }
        return result
    }

}