package kz.nedb.esb.oauth.server.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Configuration
open class WebSecurityConfig: WebSecurityConfigurerAdapter() {

    @Bean
    override fun authenticationManagerBean() =
            super.authenticationManagerBean()

    override fun configure(http: HttpSecurity) {
        http.authorizeRequests().antMatchers("/login").permitAll()
                .antMatchers("/oauth/token/revokeById/**").permitAll()
                .antMatchers("/tokens/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().permitAll()
                .and().csrf().disable()
    }

    @Bean
    open fun passwordEncoder() =
            BCryptPasswordEncoder()
}