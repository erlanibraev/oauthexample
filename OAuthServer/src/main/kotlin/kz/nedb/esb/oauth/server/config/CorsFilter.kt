package kz.nedb.esb.oauth.server.config

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
open class CorsFilter: Filter {

    override fun doFilter(req: ServletRequest?, res: ServletResponse?, chain: FilterChain?) {
        val response = res as HttpServletResponse
        response.apply {
            setHeader("Access-Control-Allow-Origin", "*")
            setHeader("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE")
            setHeader("Access-Control-Allow-Headers", "Authorization, Content-Type")
            setHeader("Access-Control-Max-Age", "3600")
        }
        if(HttpMethod.OPTIONS.name.equals((req as HttpServletRequest).method, ignoreCase = true)) {
            response.status = HttpServletResponse.SC_OK
        } else {
            chain?.doFilter(req, res)
        }
    }

}