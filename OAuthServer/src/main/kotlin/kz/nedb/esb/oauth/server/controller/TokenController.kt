package kz.nedb.esb.oauth.server.controller

import org.springframework.security.oauth2.provider.token.ConsumerTokenServices
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody

@Controller
open class TokenController(
        val tokenServices: ConsumerTokenServices,
        val tokenStore: TokenStore
) {

    @RequestMapping(
            value = ["/oauth/token/revokeById/{tokenId}"],
            method = [RequestMethod.POST]
    )
    @ResponseBody
    fun revokeToken(@PathVariable("tokenId") tokenId: String) {
        tokenServices.revokeToken(tokenId)
    }

    @RequestMapping(
            value = ["/tokens"],
            method = [RequestMethod.GET]
    )
    @ResponseBody
    fun getTokens(): List<String?>? =
            tokenStore.
                    findTokensByClientId("simpleClientId")?.
                    map { it.value }

    @RequestMapping(
            value = ["/tokens/revokeRefreshToken/{tokenId:.*}"],
            method = [RequestMethod.POST]
    )
    @ResponseBody
    fun revokeRefreshToken(@PathVariable tokenId: String): String {
        (tokenStore as? JdbcTokenStore)?.removeRefreshToken(tokenId)
        return tokenId
    }

}