package kz.nedb.esb.oauth.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class OAuthServerApplication

fun main(args: Array<String>) {
    runApplication<OAuthServerApplication>(* args)
}